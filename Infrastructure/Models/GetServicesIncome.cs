﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Models
{
    public class GetServicesIncome
    {
        public string name { get; set; } = null!;

        [Precision(15, 2)]
        public decimal income { get; set; }
    }
}
