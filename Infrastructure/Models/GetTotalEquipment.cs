﻿namespace Infrastructure.Models
{
    public class GetTotalEquipment
    {
        public string name { get; set; } = null!;
        public int amount { get; set; }
    }
}
