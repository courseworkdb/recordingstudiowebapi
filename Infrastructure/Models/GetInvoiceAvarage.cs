﻿using Microsoft.EntityFrameworkCore;
namespace Infrastructure.Models
{
    public class GetInvoiceAvarage
    {
        public int year { get; set; }
        public int month { get; set; }

        [Precision(15, 2)]
        public decimal avarage { get; set; }
    }
}
