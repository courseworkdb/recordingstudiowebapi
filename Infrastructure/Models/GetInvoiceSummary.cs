﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Models
{
    public class GetInvoiceSummary
    {
        public int year {  get; set; }
        public int month {  get; set; }

        [Precision(15, 2)]
        public decimal total {  get; set; }
    }
}
