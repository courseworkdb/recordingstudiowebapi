﻿using Domain.Entities;
using Domain.RepositoryInterfaces;

namespace Infrastructure.Repositories
{
    public class MeasureRepository : IRepository<Measure>
    {
        RecordingStudioContext _context;
        public MeasureRepository(RecordingStudioContext recordingStudioContext)
        {
            _context = recordingStudioContext;
        }

        public void Add(Measure entity)
        {
            _context.Add(entity);
            _context.SaveChanges();
        }

        public void Delete(Measure entity)
        {
           _context.Remove(entity);
           _context.SaveChanges();
        }

        public IEnumerable<Measure> GetAll()
        {
            return _context.Measures;
        }

        public Measure? GetById(int id)
        {
            return _context.Measures.FirstOrDefault(m => m.Id == id);
        }

        public void Update(Measure entity)
        {
            _context.Update(entity);
            _context.SaveChanges();
        }
    }
}
