﻿using Domain.Entities;
using Domain.RepositoryInterfaces;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class ClientRepository : IRepository<Client>
    {
        RecordingStudioContext _context;

        public ClientRepository(RecordingStudioContext context)
        {
            _context = context;
        }

        public void Add(Client entity)
        {
            _context.Clients.Add(entity);
            _context.SaveChanges(); 
        }

        public void Delete(Client entity)
        {

            _context.Clients.Remove(entity);
            _context.SaveChanges();
        }

        public IEnumerable<Client> GetAll()
        {
            return _context.Clients;
        }

        public Client? GetById(int id)
        {
            return _context.Clients.FirstOrDefault(client => client.Id == id);         
        }

        public void Update(Client entity)
        {
            _context.Clients.Update(entity);
            _context.SaveChanges();
        }
       
        public IEnumerable<Client> SearchBy(string? firstname, string? lastname, string? email, string? phonenumber)
        {
            return _context.Clients.Where(c => EF.Functions.Like(c.PhoneNumber, $"%{phonenumber}%") &&
                                              EF.Functions.Like(c.Firstname, $"%{firstname}%") &&
                                              EF.Functions.Like(c.Lastname, $"%{lastname}%") &&
                                              EF.Functions.Like(c.Email ?? string.Empty, $"%{email}%"));
        }
    }
}
