﻿using Domain.RepositoryInterfaces;
using Microsoft.EntityFrameworkCore;
using Domain.Entities;

namespace Infrastructure.Repositories
{
    public class ServiceRepository : IRepository<Service>
    {
        RecordingStudioContext _context;
        public ServiceRepository(RecordingStudioContext studioContext) 
        {
            _context = studioContext;
        }

        public void Add(Service entity)
        {
            _context.Services.Add(entity);
            _context.SaveChanges();
        }

        public void Delete(Service service)
        {           
            _context.Services.Remove(service);
            _context.SaveChanges();
        }

        public IEnumerable<Service> GetAll()
        {
            return _context.Services;
        }

        public Service? GetById(int id)
        {
            return _context.Services.FirstOrDefault(s => s.Id == id);
        }

        public void Update(Service entity)
        {
           _context.Services.Update(entity);
           _context.SaveChanges();
        }

        public IEnumerable<Service> SearchBy(string? name, decimal? minPrice, decimal? maxPrice)
        {
            return _context.Services.Where(s => EF.Functions.Like(s.Name, $"%{name}%") && 
                                                (minPrice == null|| s.Price >= minPrice) && 
                                                (maxPrice == null || s.Price <= maxPrice));
        }
    }
}
