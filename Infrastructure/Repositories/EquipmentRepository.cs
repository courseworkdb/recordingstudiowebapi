﻿using Domain.Entities;
using Domain.RepositoryInterfaces;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class EquipmentRepository : IRepository<Equipment>
    {
        RecordingStudioContext _context;
        public EquipmentRepository(RecordingStudioContext context)
        {
            _context = context;
        }

        public void Add(Equipment entity)
        {
            _context.Equipment.Add(entity);
            _context.SaveChanges();
        }

        public void Delete(Equipment entity)
        {
            _context.Equipment.Remove(entity);
            _context.SaveChanges();
        }

        public IEnumerable<Equipment> GetAll()
        {
            return _context.Equipment;
        }

        public Equipment? GetById(int id)
        {
            return _context.Equipment.FirstOrDefault(e => e.Id == id);
        }

        public void Update(Equipment entity)
        {
            _context.Update(entity);
            _context.SaveChanges();
        }

        public IQueryable CountByType()
        {
            return _context.Equipment.GroupBy(e => e.Type)
                                     .Select(group => new {type = group.Key, amount = group.Count()});
        }

        public IEnumerable<Equipment> SearchBy(string? name, string? type, int? studioId)
        {
            return _context.Equipment.Where(e => EF.Functions.Like(e.Type, $"%{type}%") &&
                                                 EF.Functions.Like(e.Name, $"%{name}%") &&
                                                 (studioId == null || e.StudioId == studioId)
                                                 );
        }
    }
}
