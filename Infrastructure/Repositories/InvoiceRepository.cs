﻿using Domain.Entities;
using Domain.RepositoryInterfaces;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class InvoiceRepository: IRepository<Invoice>
    {
        RecordingStudioContext _context;
        public InvoiceRepository(RecordingStudioContext context)
        {
            _context = context;
        }

        public void Add(Invoice entity)
        {
            Client? client = _context.Clients.FirstOrDefault(c => c.Id == entity.ClientId);
            User? user = _context.Users.FirstOrDefault(c => c.Id == entity.UserId);
            if (user == null || client == null) { return; }
            _context.Invoices.Add(entity);
            _context.SaveChanges();
        }

        public void Delete(Invoice entity)
        {
            _context.Invoices.Remove(entity);
            _context.SaveChanges();
        }

        public IEnumerable<Invoice> GetAll()
        {
            return _context.Invoices.Include(i=>i.InvoiceDetails);
        }

        public Invoice? GetById(int id)
        {
            return _context.Invoices.Include(i => i.InvoiceDetails).FirstOrDefault(x => x.Id == id);
        }

        public void Update(Invoice entity)
        {
            Invoice? invoice = GetById(entity.Id);
            if (invoice == null) return;

            _context.Invoices.Where(i => i.Id == entity.Id)
                .ExecuteUpdate(u => u
                    .SetProperty(i => i.UserId, entity.UserId)
                    .SetProperty(i => i.ClientId, entity.ClientId)
                    .SetProperty(i => i.InvoiceNumber, entity.InvoiceNumber)
                    .SetProperty(i => i.CreateDate, entity.CreateDate)
                );

            _context.InvoiceDetails.RemoveRange(invoice.InvoiceDetails);
            _context.SaveChanges();
        
            foreach (var invoiceDetail in entity.InvoiceDetails)
            {
                _context.InvoiceDetails.Add(new InvoiceDetail()
                {
                    InvoiceId = invoiceDetail.InvoiceId,
                    CreateDate = invoiceDetail.CreateDate,
                    Quantity = invoiceDetail.Quantity,
                    Price = invoiceDetail.Price,
                    ServiceId = invoiceDetail.ServiceId
                });
            }

            _context.SaveChanges();

        }

        public IEnumerable<Invoice> SearchBy(int? clientId, string? invoiceNumer, DateTime? createDate)
        {      
            return _context.Invoices.Where(i => 
                                          (createDate == DateTime.MinValue || i.CreateDate == createDate)  && 
                                          (clientId == null || i.ClientId == clientId) &&
                                          (invoiceNumer == null || i.InvoiceNumber == invoiceNumer)
                                          ).Include(i => i.InvoiceDetails);
        }
    }
}
