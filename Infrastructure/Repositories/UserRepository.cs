﻿using Domain.RepositoryInterfaces;
using Domain.Entities;

namespace Infrastructure.Repositories
{
    public class UserRepository : IRepository<User>
    {
        RecordingStudioContext _context;
        public UserRepository(RecordingStudioContext context) 
        {
            _context = context;
        }

        public void Add(User entity)
        {
            _context.Users.Add(entity);
            _context.SaveChanges();
        }

        public void Delete(User user)
        {
            _context.Users.Remove(user);    
            _context.SaveChanges();
        }

        public IEnumerable<User> GetAll()
        {
            return _context.Users;
        }

        public User? GetById(int id)
        {
            return _context.Users.FirstOrDefault(u => u.Id == id);
        }
        public User? GetByLogin(string login)
        {
            return _context.Users.FirstOrDefault(u => u.Login == login);
        }
        public void Update(User entity)
        {
            _context.Users.Update(entity);
            _context.SaveChanges();
        }
    }
}
