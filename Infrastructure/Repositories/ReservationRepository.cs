﻿using Domain.RepositoryInterfaces;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Data;
using Domain.Entities;
using System.Linq;
using System.Data.SqlTypes;

namespace Infrastructure.Repositories
{
    public class ReservationRepository : IRepository<Reservation>
    {
        private RecordingStudioContext _context;

        public ReservationRepository(RecordingStudioContext studioContext) 
        {
            _context = studioContext;
        }

        public void Add(Reservation entity)
        {
            _context.Reservations.Add(entity);
        }

        public void Delete(Reservation entity)
        {
            _context.Reservations.Remove(entity);
            _context.SaveChanges();
        }

        public IEnumerable<Reservation> GetAll()
        {
            return _context.Reservations;
        }

        public Reservation? GetById(int id)
        {
            return _context.Reservations.FirstOrDefault(reserv => reserv.Id == id);
        }

        public void Update(Reservation entity)
        {
            _context.Reservations.Update(entity);
            _context.SaveChanges();
        }

        public bool? IsAvailableStudio(int studioId)
        {
            SqlParameter studioIdParam = new("@studioId", studioId);
            SqlParameter outputParam = new("@result", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            try
            {
                _context.Database.ExecuteSqlRaw($"exec @result = usp_reservations_CheckIfAvailable @studioId", outputParam, studioIdParam);
                return (int)outputParam.Value != 0;
            }
            catch (SqlException) 
            {
                return null;
            }
        }

        public IEnumerable<Reservation> SearchBy(DateTime? startDate, DateTime? endDate, int? studioId)
        {
            startDate ??= SqlDateTime.MinValue.Value;
            endDate ??= SqlDateTime.MaxValue.Value;
            return _context.Reservations.Where(r => r.StartDate >= startDate
                                                    && r.EndDate <= endDate
                                                    && (studioId == null || r.StudioId == studioId));
        }

    }
}