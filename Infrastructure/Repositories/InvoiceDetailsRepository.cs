﻿using Domain.Entities;
using Domain.RepositoryInterfaces;
using System.Data.SqlTypes;

namespace Infrastructure.Repositories
{
    public class InvoiceDetailsRepository: IRepository<InvoiceDetail>
    {
        private RecordingStudioContext _context;

        public InvoiceDetailsRepository( RecordingStudioContext studioContext) 
        {
            _context = studioContext;
        }

        public void Add(InvoiceDetail entity)
        {
            entity.CreateDate = DateTime.Now;
            _context.Add(entity);
            _context.SaveChanges();
        }

        public void Delete(InvoiceDetail entity)
        {
            _context.Remove(entity);
        }

        public IEnumerable<InvoiceDetail> GetAll()
        {
            return _context.InvoiceDetails;
        }

        public InvoiceDetail? GetById(int id)
        {
            return _context.InvoiceDetails.FirstOrDefault(x => x.Id == id);
        }

        public void Update(InvoiceDetail entity)
        {
            _context.Update(entity);
            _context.SaveChanges();
        }
    }
}
