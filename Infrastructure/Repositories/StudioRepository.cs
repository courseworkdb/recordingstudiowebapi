﻿using Domain.Entities;
using Domain.RepositoryInterfaces;

namespace Infrastructure.Repositories
{
    public class StudioRepository : IRepository<Studio>
    {
        RecordingStudioContext _context;
        public StudioRepository(RecordingStudioContext context)
        {
            _context = context;
        }

        public void Add(Studio entity)
        {
            _context.Studios.Add(entity);
            _context.SaveChanges();
        }

        public void Delete(Studio entity)
        {
           _context.Remove(entity);
           _context.SaveChanges();
        }

        public IEnumerable<Studio> GetAll()
        {
            return _context.Studios;
        }

        public Studio? GetById(int id)
        {
            return _context.Studios.FirstOrDefault(s => s.Id == id);
        }

        public void Update(Studio entity)
        {
            _context.Update(entity);
            _context.SaveChanges();
        }


    }
}
