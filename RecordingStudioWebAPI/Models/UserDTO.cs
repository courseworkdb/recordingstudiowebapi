﻿using System.ComponentModel.DataAnnotations;

namespace RecordingStudioWebAPI.Models
{
    public class UserDTO
    {
        [Required]
        public string? Password { get; set; } = null!;

        [Required]
        public string? Login { get; set; } = null!;
    }
}
