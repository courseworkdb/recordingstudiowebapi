﻿using Domain.Entities;
using Infrastructure;
using Infrastructure.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace RecordingStudioWebAPI.Controller
{
    [Route("[controller]")]
    [ApiController]
    public class InvoiceDetailsController : ControllerBase
    {
        InvoiceDetailsRepository _detailsRepository;
        public InvoiceDetailsController(RecordingStudioContext studioContext) 
        {
            _detailsRepository = new InvoiceDetailsRepository(studioContext);
        }

        [HttpGet]
        public IEnumerable<InvoiceDetail> Index()
        {
            return _detailsRepository.GetAll();
        }

        [HttpGet("{id}")]
        public ActionResult<InvoiceDetail> Get(int id)
        {
            InvoiceDetail? invoiceDetail = _detailsRepository.GetById(id);
            return invoiceDetail == null ? NotFound() : invoiceDetail;
        }

        [HttpPost]
        public ActionResult<Invoice> Add(InvoiceDetail invoiceDetail)
        {
            _detailsRepository.Add(invoiceDetail);
            return CreatedAtAction(nameof(Add), new { id = invoiceDetail.Id }, invoiceDetail);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            InvoiceDetail? invoiceDetail = _detailsRepository.GetById(id);
            if (invoiceDetail == null)
            {
                return NotFound();
            }
            _detailsRepository.Delete(invoiceDetail);
            return NoContent();
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, InvoiceDetail invoiceDetail)
        {
            if (id != invoiceDetail.Id)
                return BadRequest();
            _detailsRepository.Update(invoiceDetail);
            return NoContent();
        }
    }
}
