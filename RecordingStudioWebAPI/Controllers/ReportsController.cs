﻿using Domain.Entities;
using Infrastructure;
using Infrastructure.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace RecordingStudioWebAPI.Controller
{
    [Route("[controller]")]
    [ApiController]
    public class ReportsController : ControllerBase
    {
        RecordingStudioContext _context;
        public ReportsController(RecordingStudioContext studioContext) 
        {
            _context = studioContext;
        }

        [HttpGet("EquipmentByStudio")]
        public IQueryable CountEquipmentByStudio([FromQuery] string? type) 
        {
            try
            {
                return _context.GetTotalEquipment.FromSqlInterpolated($"SELECT * FROM GetTotalEquipment({type})");

            }
            catch(SqlException)
            {
                return Enumerable.Empty<GetTotalEquipment>().AsQueryable();
            }
        }

        [HttpGet("Invoices/Sum")]
        public IActionResult GroupSum([FromQuery] DateTime startDate, [FromQuery] DateTime endDate)
        {
            try
            {
                if (startDate >= endDate)
                    return BadRequest(new {message ="StartDate need to be eralier then EndDate"});
                var query = _context.GetInvoiceSummary.FromSqlInterpolated($"SELECT * FROM GetInvoiceSummary({startDate}, {endDate})");
                return Ok(query);

            }
            catch (SqlException)
            {
                return BadRequest();
            }
        }

        [HttpGet("Invoices/Avg")]
        public IActionResult GroupAvg([FromQuery] DateTime startDate, [FromQuery] DateTime endDate)
        {
            try
            {
                if (startDate >= endDate)
                    return BadRequest(new { message = "StartDate need to be eralier then EndDate" });
                var query = _context.GetInvoiceAvarage.FromSqlInterpolated($"SELECT * FROM GetInvoiceAvarage({startDate}, {endDate})");
                return Ok(query);
            }
            catch (SqlException)
            {
                return BadRequest();
            }
        }

        [HttpGet("Services")]
        public IActionResult ServicesIncome([FromQuery] DateTime startDate, [FromQuery] DateTime endDate)
        {
            try
            {
                if (startDate >= endDate)
                    return BadRequest(new { message = "StartDate need to be eralier then EndDate" });
                var query = _context.GetServicesIncome.FromSqlInterpolated($"SELECT * FROM GetServicesIncome({startDate}, {endDate})");
                return Ok(query);
            }
            catch (SqlException)
            {
                return BadRequest();
            }

        }
    }
}
