﻿using Infrastructure;
using Infrastructure.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Domain.Entities;

namespace RecordingStudioWebAPI.Controller
{
    [Route("[controller]")]
    [ApiController]
    public class ServicesController : ControllerBase
    {
        ServiceRepository _serviceRepository;
        public ServicesController(RecordingStudioContext studioContext)
        {
            _serviceRepository = new ServiceRepository(studioContext);
        }

        [HttpGet]
        public IEnumerable<Service> Index()
        {
            return _serviceRepository.GetAll();
        }

        [HttpGet("{id}")]
        public ActionResult<Service> Get(int id)
        {
            Service? service = _serviceRepository.GetById(id);
            return service == null ? NotFound() : service;
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            Service? service;
            if ((service = _serviceRepository.GetById(id)) != null)
            {
                _serviceRepository.Delete(service);
                return NoContent();
            }
            return NotFound();
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id,Service service)
        {
            if (id != service.Id)
                return BadRequest();
            _serviceRepository.Update(service);
            return NoContent();
        }

        [HttpPost]
        public IActionResult Add(Service service)
        {
            if (service == null)
                return BadRequest();
            _serviceRepository.Add(service);
            return CreatedAtAction(nameof(Add), new { id = service.Id }, service);
        }

        [HttpGet("Search")]
        public IActionResult SearchBy([FromQuery] string? name, [FromQuery] decimal? minPrice, [FromQuery] decimal? maxPrice)
        {
            return Ok(_serviceRepository.SearchBy(name,minPrice,maxPrice));
        }
    }
}
