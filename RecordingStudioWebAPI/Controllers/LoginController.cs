﻿using Domain.Entities;
using Infrastructure;
using Infrastructure.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using RecordingStudioWebAPI.Models;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace RecordingStudioWebAPI.Controller
{
    [Route("[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        UserRepository userRepository;
        private IConfiguration _config;
        public LoginController(RecordingStudioContext studioContext, IConfiguration configuration)
        {
            userRepository = new UserRepository(studioContext);
            _config = configuration;
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult Index(UserDTO userInfo)
        {
            User? user = userRepository.GetByLogin(userInfo.Login);
            if (user == null) { return Unauthorized(); }
            if (user.Password != userInfo.Password) return Forbid();
            return Ok(new {token = GenerateToken() } );
        }

        private string GenerateToken()
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
                                             _config["Jwt:Issuer"],
                                             null,
                                             expires: DateTime.Now.AddMinutes(120),
                                             signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
