﻿using Infrastructure;
using Infrastructure.Repositories;
using Microsoft.AspNetCore.Mvc;
using Domain.Entities;
using System.Data.SqlTypes;
using Microsoft.AspNetCore.Authorization;

namespace RecordingStudioWebAPI.Controller
{
    [Route("[controller]")]
    [ApiController]
    public class ClientsController : ControllerBase
    {
        ClientRepository _clientRepository;

        public ClientsController(RecordingStudioContext studioContext)
        {
            _clientRepository = new ClientRepository(studioContext);
        }

        [HttpGet]
        public IEnumerable<Client> Index()
        {
            return _clientRepository.GetAll();
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            Client? client = _clientRepository.GetById(id);
            if (client == null)
            {
                return NotFound();
            }

            return Ok(client);
        }

        [HttpPost]
        public IActionResult Add(Client client)
        {
            if (client == null)
                return BadRequest();

            _clientRepository.Add(client);
            return CreatedAtAction(nameof(Add), new { id = client.Id }, client);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, Client client)
        {
            if (id != client.Id)
                return BadRequest();
            _clientRepository.Update(client);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteById(int id)
        {
            Client? client = _clientRepository.GetById(id);
            if (client == null)
            {
                return NotFound();
            }
            _clientRepository.Delete(client);
            return NoContent();
        }

        [HttpGet("Search")]
        public IActionResult Search([FromQuery] string? firstname,
                                                     [FromQuery] string? lastname,
                                                     [FromQuery] string? email,
                                                     [FromQuery] string? phonenumber)
        {
            try
            {
                return Ok(_clientRepository.SearchBy(firstname, lastname, email, phonenumber));
            }
            catch (SqlTypeException)
            {
                return BadRequest();
            }
        }
    }

}