﻿using Domain.Entities;
using Infrastructure;
using Infrastructure.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace RecordingStudioWebAPI.Controller
{
    [Route("[controller]")]
    [ApiController]
    public class EquipmentController : ControllerBase
    {
        EquipmentRepository _equipmentRepository;
        public EquipmentController(RecordingStudioContext studioContext)
        {
            _equipmentRepository = new EquipmentRepository(studioContext); 
        }

        [HttpGet]
        public IEnumerable<Equipment> Index()
        {
            return _equipmentRepository.GetAll();
        }

        [HttpGet("{id}")]
        public ActionResult<Equipment> GetById(int id)
        {
            Equipment? equipment = _equipmentRepository.GetById(id);
            return equipment == null ? NotFound() : equipment;
        }

        [HttpGet("Search")]
        public IEnumerable<Equipment> GetByStudioId([FromQuery] string? name, [FromQuery] string? type, [FromQuery] int? studioId)
        {
            return _equipmentRepository.SearchBy(name, type, studioId);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, Equipment equipment)
        {
            if (id != equipment.Id)
                return BadRequest();
            _equipmentRepository.Update(equipment);
            return NoContent();
        }

        [HttpDelete("{id}")] 
        public IActionResult Delete(int id)
        {
            Equipment? equipment = _equipmentRepository.GetById(id);
            if (equipment == null)
            {
                return NotFound();
            }
            _equipmentRepository.Delete(equipment);
            return NoContent();
        }

        [HttpPost]
        public IActionResult Add(Equipment equipment)
        {
            _equipmentRepository.Add(equipment);
            return CreatedAtAction(nameof(Add), new { id = equipment.Id }, equipment);
        }

        [HttpGet("Type/Count")]
        public IQueryable CountByType() 
        {
            return _equipmentRepository.CountByType();
        }

    }
}
