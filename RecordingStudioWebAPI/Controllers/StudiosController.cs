﻿using Domain.Entities;
using Infrastructure;
using Infrastructure.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace RecordingStudioWebAPI.Controller
{
    [Route("[controller]")]
    [ApiController]
    public class StudiosController : ControllerBase
    {
        private StudioRepository _studioRepository;
        public StudiosController(RecordingStudioContext studioContext) 
        {
            _studioRepository = new StudioRepository(studioContext);
        }

        [HttpGet]
        public IEnumerable<Studio> Index()
        {
            return _studioRepository.GetAll();
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            Studio? studio = _studioRepository.GetById(id);
            if (studio == null)
            {
                return NotFound();
            }
            return Ok(studio);
        }

        [HttpPost]
        public IActionResult Add(Studio studio)
        {
            if (studio == null)
                return BadRequest();

            _studioRepository.Add(studio);
            return CreatedAtAction(nameof(Add), new { id = studio.Id }, studio);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, Studio studio)
        {
            if (id != studio.Id)
                return BadRequest();
            _studioRepository.Update(studio);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteById(int id)
        {
            Studio? studio = _studioRepository.GetById(id);
            if (studio == null)
            {
                return NotFound();
            }
            _studioRepository.Delete(studio);
            return NoContent();
        }
    }
}
