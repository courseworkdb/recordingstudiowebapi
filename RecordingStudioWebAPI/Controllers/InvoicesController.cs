﻿using Domain.Entities;
using Infrastructure;
using Infrastructure.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using System.Data.SqlTypes;

namespace RecordingStudioWebAPI.Controller
{
    [Route("[controller]")]
    [ApiController]
    public class InvoicesController : ControllerBase
    {
        InvoiceRepository _invoiceRepository;
        public InvoicesController(RecordingStudioContext studioContext) 
        {
            _invoiceRepository = new InvoiceRepository(studioContext);
        }

        [HttpGet]
        public IEnumerable<Invoice> Index() 
        {
            return _invoiceRepository.GetAll();
        }

        [HttpGet("{id}")]
        public ActionResult<Invoice> Get(int id)
        {
            Invoice? invoice = _invoiceRepository.GetById(id);
            return invoice == null ? NotFound() : invoice;
        }
       
        [HttpPost]
        public ActionResult<Invoice> Add(Invoice invoice)
        {
            try
            {
                _invoiceRepository.Add(invoice);
                if (invoice.Id == 0) return BadRequest();
                return CreatedAtAction(nameof(Add), new { id = invoice.Id }, invoice);
            }catch(SqlException)
            {
                return BadRequest();
            }
            
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            Invoice? invoice = _invoiceRepository.GetById(id);
            if (invoice == null)
            {
                return NotFound();
            }
            _invoiceRepository.Delete(invoice);
            return NoContent();
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, Invoice invoice)
        {
            if (id != invoice.Id)
                return BadRequest();
            _invoiceRepository.Update(invoice);
            return NoContent();
        }


        [HttpGet("{id}/items")]
        public IActionResult GetDetails(int id)
        {
            Invoice? invoice = _invoiceRepository.GetById(id);
            return invoice == null ? NotFound() : Ok(invoice.InvoiceDetails);
        }

        [HttpGet("{id}/items/{invoiceDetailId}")]
        public IActionResult GetInvoiceDetail(int id,int invoiceDetailId)
        {
            Invoice? invoice = _invoiceRepository.GetById(id);
            if (invoice == null) return NotFound(new {message = "Invoice is not found" });
            InvoiceDetail? detail = invoice.InvoiceDetails.SingleOrDefault(i => i.Id == invoiceDetailId);
            return detail == null ? NotFound(new {message="Item is not found"}) : Ok(detail);
        }

        [HttpGet("Search")]
        public IActionResult Search([FromQuery] string? invoiceNumber,
                                    [FromQuery] DateTime createDate,
                                    [FromQuery] int? clientId )
        {
            try
            {
                return Ok(_invoiceRepository.SearchBy(clientId, invoiceNumber, createDate));
            }
            catch (SqlTypeException)
            {
                return BadRequest();
            }
        }
    }
}
