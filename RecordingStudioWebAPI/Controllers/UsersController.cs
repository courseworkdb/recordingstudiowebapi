﻿using Domain.RepositoryInterfaces;
using Infrastructure;
using Infrastructure.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Domain.Entities;

namespace RecordingStudioWebAPI.Controller
{
    [Route("[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        IRepository<User> _userRepository;

        public UsersController(RecordingStudioContext studioContext)
        {
            _userRepository = new UserRepository(studioContext);
        }

        [HttpGet]
        public IEnumerable<User> Index()
        {
           return _userRepository.GetAll();
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            User ?user = _userRepository.GetById(id);
            return user == null ? NotFound() : Ok(user);
        }

        [HttpPost]
        public ActionResult<User> Add(User user)
        {
            _userRepository.Add(user);
            return CreatedAtAction(nameof(Add), new { id = user.Id }, user);
        }

        [HttpPut("{id}")]
        public ActionResult<User> Update(int id,User user)
        {
            if (id != user.Id )
                return BadRequest();
            _userRepository.Update(user);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteById(int id)
        {
            User? user = _userRepository.GetById(id);
            if (user == null)
            {
                return NotFound();
            }
            _userRepository.Delete(user);
            return NoContent();
        }
    }
}
