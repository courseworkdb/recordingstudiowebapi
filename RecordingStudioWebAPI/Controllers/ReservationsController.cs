﻿using Infrastructure;
using Infrastructure.Repositories;
using Microsoft.AspNetCore.Mvc;
using Domain.Entities;
using System.Data.SqlTypes;

namespace RecordingStudioWebAPI.Controller
{
    [Route("[controller]")]
    [ApiController]
    public class ReservationsController : ControllerBase
    {
        ReservationRepository _reservationRepository;
        public ReservationsController(RecordingStudioContext studioContext) 
        {
            _reservationRepository = new ReservationRepository(studioContext);
        }

        [HttpGet]
        public IEnumerable<Reservation> Index() 
        {
            return _reservationRepository.GetAll();
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var reservation = _reservationRepository.GetById(id);
            if (reservation == null)
                return NotFound();
            else
                return Ok(reservation);

        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, Reservation reservation)
        {
            if (id != reservation.Id)
                return BadRequest();
            _reservationRepository.Update(reservation);
            return NoContent();
        }

        [HttpPost]
        public IActionResult Add(Reservation reservation)
        {
            _reservationRepository.Add(reservation);
            return CreatedAtAction(nameof(Add), new { id = reservation.Id }, reservation);
        }

        [HttpGet("IsAvailable/{studioId}")]
        public IActionResult? IsAvailableStudio(int studioId)
        {
            bool? res = _reservationRepository.IsAvailableStudio(studioId);
            return Ok(res);
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteById(int id)
        {
            Reservation? reservation = _reservationRepository.GetById(id);
            if (reservation == null)
            {
                return NotFound();
            }
            _reservationRepository.Delete(reservation);
            return NoContent();
        }

        [HttpGet("Search")]
        public ActionResult<IEnumerable<Reservation>> SearchBy([FromQuery] DateTime? started, [FromQuery] DateTime? ended, [FromQuery] int? studioId)
        {
            try
            {
                return _reservationRepository.SearchBy(started, ended, studioId).ToList();
            }
            catch(SqlTypeException)
            {
                return BadRequest();
            }
        }
    }
}
