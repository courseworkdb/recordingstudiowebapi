﻿using Domain.Entities;
using Infrastructure;
using Infrastructure.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace RecordingStudioWebAPI.Controller
{
    [Route("[controller]")]
    [ApiController]
    public class MeasuresController : ControllerBase
    {
        MeasureRepository _measureRepository;

        public MeasuresController(RecordingStudioContext studioContext) 
        {
            _measureRepository = new MeasureRepository(studioContext);
        }

        [HttpGet]
        public IEnumerable<Measure> Index()
        {
            return _measureRepository.GetAll();
        }

        [HttpGet("{id}")]
        public ActionResult<Measure> Get(int id)
        {
            Measure? measure = _measureRepository.GetById(id);
            return measure == null ? NotFound() : measure;
        }

        [HttpDelete] 
        public ActionResult Delete(int id)
        {
            Measure? measure = _measureRepository.GetById(id);
            if (measure == null)
            {
                return NotFound();
            }
            _measureRepository.Delete(measure);
            return NoContent();
        }

        [HttpPut("{id}")]
        public ActionResult Update(int id, Measure measure)
        {
            if (id != measure.Id)
                return BadRequest();
            _measureRepository.Update(measure);
            return NoContent();
        }
    }
}
