﻿using System.Text.Json.Serialization;

namespace Domain.Entities;

public partial class Service: Entity
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public decimal Price { get; set; }

    public int MeasureId { get; set; }

    [JsonIgnore]
    public virtual ICollection<InvoiceDetail> InvoiceDetails { get; set; } = new List<InvoiceDetail>();

    [JsonIgnore]
    public Measure? Measure { get; set; }
}
