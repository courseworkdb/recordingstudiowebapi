﻿using System.Text.Json.Serialization;

namespace Domain.Entities;

public partial class Equipment : Entity
{
    public int Id { get; set; }

    public string Type { get; set; } = null!;

    public string Name { get; set; } = null!;

    public int StudioId { get; set; }

    [JsonIgnore]
    public Studio? Studio { get; set; } 
}
