﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Domain.Entities;

public partial class Measure : Entity
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public string? ShortName { get; set; }

    [JsonIgnore]
    public virtual ICollection<Service> Services { get; set; } = new List<Service>();
}
