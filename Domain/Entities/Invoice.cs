﻿using System.Text.Json.Serialization;

namespace Domain.Entities;
public partial class Invoice : Entity
{
    public int Id { get; set; }

    public decimal? TotalPrice { get; set; }

    public DateTime CreateDate { get; set; }

    public string? InvoiceNumber { get; set; }

    public int ClientId { get; set; }

    public int UserId { get; set; }

    [JsonIgnore]
    public  Client? Client { get; set; }

    public  ICollection<InvoiceDetail> InvoiceDetails { get; set; } = new List<InvoiceDetail>();

    [JsonIgnore]
    public  User? User { get; set; }     
}
