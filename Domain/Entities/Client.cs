﻿using System.Text.Json.Serialization;

namespace Domain.Entities;

public partial class Client : Entity
{
    public int Id { get; set; }

    public string Firstname { get; set; } = null!;

    public string Lastname { get; set; } = null!;

    public string? Email { get; set; }

    public string PhoneNumber { get; set; } = null!;

    [JsonIgnore]
    public ICollection<Invoice> Invoices { get; set; } = new List<Invoice>();

    [JsonIgnore]
    public ICollection<Reservation> Reservations { get; set; } = new List<Reservation>();
}
