﻿using System.Text.Json.Serialization;

namespace Domain.Entities;

public partial class InvoiceDetail : Entity
{
    public int Id { get; set; }

    public decimal? Price { get; set; }

    public decimal Quantity { get; set; }

    public DateTime CreateDate { get; set; }

    public int ServiceId { get; set; }

    public int InvoiceId { get; set; }

    [JsonIgnore]
    public Invoice? Invoice { get; set; }

    [JsonIgnore]
    public Service? Service { get; set; }
}
