﻿namespace Domain.RepositoryInterfaces
{
    public interface IRepository<T> where T : Entity
    {
        T? GetById(int id);
        IEnumerable<T> GetAll();
        void Update(T entity);
        void Delete(T entity);
        void Add(T entity);
    }
}
